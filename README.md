![Classic Banner fake fire.png](https://bitbucket.org/repo/je46zM/images/2644786145-Classic%20Banner%20fake%20fire.png)

# About the Mod #

Fake fire adds in a block that looks like fire, but dose not have the risk of burning down your creations. the mod also adds in a flint and steel which is used to spawn in the fire. to remove the fire block just simply hit it like any other block. fake fire is an ideal for replacing the original fire in minetest

### How To Install ###

## Linux ##
1. Go to your Home directory
2. You should see a folder called minetest. If not the folder is probely hidden. To get to it type "/.minetest/" in the navigation bar.
3. You should now be in a folder called ".minetest". in this folder you should see another folder called "mods". If you dont see this make a new folder called "mods".
4.   Copy and Paste the mod into the "mods" folder. (make sure that it is extracted from the zip file". 

## Windows ##
1. Locate where the game has been saved. if you don't know where it has been saved right click the minetest icon and press "Open file location"
2. when you are in the minetest folder you should see a folder called "mods", as well as other folders like "bin", "textures" , "games", ect. 
3. Copy and Paste the mod into the "mods" folder. (make sure that it is extracted from the zip file". 

### More Info ###
[ website](http://thatraspberrypiserver.raspberryip.com/Infinatum_Minetest)