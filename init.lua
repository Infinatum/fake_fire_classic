-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~ --
-- FAKE FIRE MOD - Version 1.0 --
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~ --

fake_fire_water = true;

-- function for fire --
local function set_fake_fire(pointed_thing, itemstack, wear)
		local n = minetest.env:get_node(pointed_thing.above)

    -- if the node is air then --
		if n.name ~= ""  and n.name == "air" then

      -- create fire above that node --
			minetest.env:set_node(pointed_thing.above, {name="fake_fire:fake_fire"})
      -- then play the sound --
      minetest.sound_play("fake_fire_light", {
        pos = pointed_thing.above,
        max_hear_distance = 10,
        gain = 10.0,
      })
      -- degrade the item --
      itemstack:add_wear(wear)

    end
end

if fake_fire_water == true then
  minetest.register_abm({
  	nodenames = {"fake_fire:fake_fire"},
  	neighbors = {"group:puts_out_fire"},
  	interval = 3,
  	chance = 1,
  	catch_up = false,
  	action = function(p0, node, _, _)
  		minetest.remove_node(p0)
  		minetest.sound_play("fake_fire_extinguish",
  			{pos = p0, max_hear_distance = 10, gain = 0.25})
  	end,
  })
end

-- ========== --
-- Fire Nodes --
-- ========== --

minetest.register_node("fake_fire:fake_fire", {
  description = "fake_fire",
	tiles = {
		{name="fake_fire_animated.png",
    animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=1.5}},
		},
	paramtype = "light",
	is_ground_content = true,
    inventory_image = 'fake_fire.png',
	drawtype = "firelike",
  buildable_to = true,
	light_source = 14,
	drop = '', -- fire cannot be picked up
	damage_per_second = 0, -- default 0 Recomended 2*0.5, --
	groups = {dig_immediate=3,attached_node=1},
	paramtype = "light",
	walkable = false,
  on_punch = function(pos, node, puncher)
    --  local name = minetest.get_node(pos).name
	   minetest.sound_play("fake_fire_extinguish",
	   {pos = pos, gain = 1.0, max_hear_distance = 10,})
  end
})

-- ========== --
-- Fire Tools --
-- ========== --

-- Flint and Steel --

minetest.register_tool("fake_fire:flint_and_steel", {
	description = "Flint & Steel",
	inventory_image = "flint_and_steel.png",
	liquids_pointable = false,
	stack_max = 1,
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=0,
		groupcaps={
			flamable = {uses=65, maxlevel=1},
		}
	},
	on_use = function(itemstack, user, pointed_thing)
    -- if item is pointing at a node then --
    if pointed_thing.type == "node" then
      -- set a value the degradeing --
      wear = 65535/65
      -- load function on line 6 --
  		set_fake_fire(pointed_thing,itemstack,wear)
      return itemstack
  	end
  end,
})

-- Old Flint and Steel --

--[[ Players cannot craft this item, they will need to spawn it via
     "/give" or creative.
]]--

minetest.register_tool("fake_fire:old_flint_and_steel", {
	description = "Never Ending Flint & Steel",
	inventory_image = "flint_and_steel.png",
	liquids_pointable = false,
	stack_max = 1,
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=0,
		groupcaps={
			flamable = {uses=65, maxlevel=1},
		}
	},
	on_use = function(itemstack, user, pointed_thing)
    if pointed_thing.type == "node" then
      -- this is so that it will never brake --
      wear = 0
      -- load function on line 6 --
  		set_fake_fire(pointed_thing, itemstack, wear)
      return itemstack
  	end
  end,
})

-- =========== --
-- Fire crafts --
-- =========== --

minetest.register_craft({
	output = "fake_fire:flint_and_steel",
	recipe = {
		{"default:flint", ""},
		{"", "default:steel_ingot"},
	}
})
